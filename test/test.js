const assert = require('assert')
const fs = require('fs')
const {promisify} = require('util')

const unlink = promisify(fs.unlink)
const readFile = promisify(fs.readFile)

const config = require('../index.js')
const {Config} = config

const data = {
	'boolean false': false,
	'boolean true': true,
	'number': 123,
	'number zero': 0,
	'array': ['hi'],
	'array zero': [],
	'string': 'hi',
	'string empty': '',
	'object': {x:'y'},
	'object empty': {}
}
const testData = []
for(const f in data){
	testData.push({
		key: f,
		value: data[f]
	})
}

const cleanup = async function(){
	await config.clear()
	try{
		await unlink(config.file)
	}catch(err){}
}

describe('set', function(){
	before('load', async function(){
		await config.load()
	})
	after(cleanup)
	describe('new', function(){
		testData.forEach(function(test){
			it(test.key, async function(){
				await config.set(test.key, test.value)
				const data = JSON.parse(await readFile(config.file, 'utf8'))
				assert.deepEqual(data[test.key], test.value)
			})
		})
	})
	describe('overwrite', function(){
		before(async function(){
			await config.save()
			await config.load()
		})
		testData.forEach(function(test){
			it(test.key, async function(){
				await config.set(test.key, test.value)
				const data = JSON.parse(await readFile(config.file, 'utf8'))
				assert.deepEqual(data[test.key], test.value)
			})
		})
	})
	describe('depth', function(){
		after(async function(){
			await config.remove('obj')
		})
		it('new', async function(){
			await config.set('obj.obj2.obj3.v', '123')
			const data = JSON.parse(await readFile(config.file, 'utf8'))
			assert.deepEqual(data.obj.obj2.obj3.v, '123')
			assert.deepEqual(data.obj, {
				obj2: {obj3: {v: '123'} }
			})
		})
		it('partial overwrite', async function(){
			await config.set('obj.obj2.obj3.b', '1234')
			const data = JSON.parse(await readFile(config.file, 'utf8'))
			assert.deepEqual(data.obj.obj2.obj3.b, '1234')
			assert.deepEqual(data.obj, {
				obj2: {obj3: {v: '123', b: '1234'} }
			})
		})
		it('complete overwrite', async function(){
			await config.set('obj.obj2', '1234')
			const data = JSON.parse(await readFile(config.file, 'utf8'))
			assert.deepEqual(data.obj.obj2, '1234')
			assert.deepEqual(data.obj, {obj2: '1234'})
		})
	})
})
describe('get', function(){
	const config = new Config('./test/config.json')
	before('load existing', async function(){
		await config.load()
	})
	describe('fresh', function(){
		testData.forEach(function(test){
			it(test.key, function(){
				assert.deepEqual(config.get(test.key), test.value)
			})
		})
	})
	describe('reloaded', function(){
		before(async function(){
			await config.load()
		})
		testData.forEach(function(test){
			it(test.key, function(){
				assert.deepEqual(config.get(test.key), test.value)
			})
		})
	})
	it('all', function(){
		assert.deepEqual(config.get(), data)
	})
})
describe('defaults', function(){
	describe('load lifecycle', function(){
		afterEach(async function(){
			await config.clearDefaults()
			await cleanup()
		})
		it('set and not written', async function(){
			await config.default('default', 'value')
			let data = {}
			try{
				data = JSON.parse(await readFile(config.file, 'utf8'))
			}catch(err){}
			
			assert.strictEqual(data.default, undefined)
			assert.deepEqual(config.raw.default, 'value')
			assert.deepEqual(config.defaults.default, 'value')
			assert.deepEqual(config.get('default'), 'value')
		})
		it('load', async function(){
			await config.default('default', 'value1')
			await config.default('overwriteme', 'value2')
			
			await config.load()
			await config.set('overwriteme', 'value3')
			const data = JSON.parse(await readFile(config.file, 'utf8'))
			
			assert.deepEqual(data.default, 'value1')
			assert.deepEqual(config.raw.default, 'value1')
			assert.deepEqual(config.defaults.default, 'value1')
			assert.deepEqual(config.get('default'), 'value1')
			
			assert.deepEqual(data.overwriteme, 'value3')
			assert.deepEqual(config.raw.overwriteme, 'value3')
			assert.deepEqual(config.defaults.overwriteme, 'value2')
			assert.deepEqual(config.get('overwriteme'), 'value3')
		})
	})
	describe('loaded', function(){
		before('load', async function(){
			await config.load()
		})
		after(cleanup)
		it('set default when no file', async function(){
			await config.default('default', 'value')
			const data = JSON.parse(await readFile(config.file, 'utf8'))
			assert.deepEqual(config.raw.default, 'value')
			assert.deepEqual(data.default, 'value')
		})
		it('sets default when file', async function(){
			await config.default('default2', 'value2')
			const data = JSON.parse(await readFile(config.file, 'utf8'))
			assert.deepEqual(config.raw.default, 'value')
			assert.deepEqual(config.raw.default2, 'value2')
			assert.deepEqual(data.default, 'value')
			assert.deepEqual(data.default2, 'value2')
		})
		it('adding default for existing key', async function(){
			await config.set('exists', 'yep')
			await config.default('exists', 'default')
			const data = JSON.parse(await readFile(config.file, 'utf8'))
			assert.deepEqual(config.raw.exists, 'yep')
			assert.deepEqual(data.exists, 'yep')
		})
		it('changing a default\'s value', async function(){
			await config.set('default', 'set')
			const data = JSON.parse(await readFile(config.file, 'utf8'))
			assert.deepEqual(config.raw.default, 'set')
			assert.deepEqual(data.default, 'set')
		})
	})
})
describe('subconfig', function(){
	let sub
	before('load', async function(){
		await config.load()
		sub = config.getSub('sub')
	})
	after(cleanup)
	it('set new', async function(){
		await sub.set(undefined, {
			key: 'value2'
		})
		const data = JSON.parse(await readFile(config.file, 'utf8'))
		assert.deepEqual(data.sub, {
			key: 'value2'
		})
	})
	it('set overwrite', async function(){
		await sub.set(undefined, {overwrite: true})
		const data = JSON.parse(await readFile(config.file, 'utf8'))
		assert.deepEqual(data.sub, {overwrite: true})
	})
	it('get', function(){
		const data = sub.get('overwrite')
		assert.deepEqual(data, true)
	})
	it('remove', async function(){
		await sub.remove()
		const data = JSON.parse(await readFile(config.file, 'utf8'))
		assert.deepEqual(data.sub, undefined)
	})
	it('default', async function(){
		await sub.default(undefined, {default: true})
		const data = JSON.parse(await readFile(config.file, 'utf8'))
		assert.deepEqual(data.sub, {default: true})
	})
})
//TODO sometime later, maybe
describe('events', function(){
	describe('on change', function(){
		it('add new')
		it('change')
		it('change to the same')
		it('add new deep')
		it('change deep\'s deeper')
		it('change deep')
	})
	it('pre save run')
	it('loaded run')
	it('post load run')
})
